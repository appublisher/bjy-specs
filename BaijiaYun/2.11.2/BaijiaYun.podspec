Pod::Spec.new do |s|

  s.name         = "BaijiaYun"
  s.version      = "2.11.2"
  s.summary      = "BaijiaYun SDK."
  s.description  = "All iOS framework for BaijiaYun."

  s.homepage     = "https://www.baijiayun.com/"
  s.license      = { :type => "MIT", :file => "LICENSE" }
  s.author       = { "MingLQ" => "minglq.9@gmail.com" }

  s.platform     = :ios, "9.0"
  s.ios.deployment_target = "9.0"
  s.xcconfig     = {"ENABLE_BITCODE" => "NO"}

  s.source       = { :http => "https://downsdk.baijiayun.com/ios/BaijiaYun/BaijiaYun-2.11.2.zip",
                     :sha256 => "02bed5fb0da0f7d57c74758bba6451eb4caf9d131a5fc093024205c709dece58" }

  s.pod_target_xcconfig = {
    "GCC_PREPROCESSOR_DEFINITIONS" => "$(inherited) BJLIVECORE_VERSION=#{s.version} 
                                                    BJLIVEUI_VERSION=#{s.version}
                                                    BJLSellUI=#{s.version}
                                                    BJVIDEOPLAYERCORE_VERSION=#{s.version}
                                                    BJPLAYBACKUI_VERSION=#{s.version}
                                                    BJVIDEOPLAYERUI_VERSION=#{s.version}"
  }

  s.default_subspecs = ["BJLiveCore"]

  s.subspec "BJLiveCore" do |ss|
    ss.dependency "BaijiaYun/BJLiveBase"
    ss.dependency "BaijiaYun/_BJLiveCore"
    ss.dependency "BaijiaYun/BJYRTCEngine"
    ss.dependency "BaijiaYun/Tencent"
    ss.dependency "BaijiaYun/Agora"
  end

  s.subspec "BJLiveUI" do |ss|
    ss.dependency "BaijiaYun/_BJLiveUI"
    ss.dependency "BaijiaYun/BJLiveCore"
    ss.dependency "QBImagePickerController", "~> 3.0"
  end

  s.subspec "BJLSellUI" do |ss|
    ss.dependency "BaijiaYun/_BJLSellUI"
    ss.dependency "BaijiaYun/BJLiveCore"
  end

  s.subspec "BJVideoPlayerCore" do |ss|
    ss.dependency "BaijiaYun/_BJVideoPlayerCore"
    ss.dependency "BaijiaYun/BJLiveCore"
  end

  s.subspec "BJVideoPlayerUI" do |ss|
    ss.dependency "BaijiaYun/_BJVideoPlayerUI"
    ss.dependency "BaijiaYun/BJVideoPlayerCore"
  end

  s.subspec "BJPlaybackUI" do |ss|
    ss.dependency "BaijiaYun/_BJPlaybackUI"
    ss.dependency "BaijiaYun/BJVideoPlayerCore"
  end

  ################ internal subspecs ################

  s.subspec "BJLiveBase" do |ss|
    ss.source_files         = [
      "frameworks/BJLiveBase.framework/Versions/A/Headers/**/*.h"
    ]
    ss.public_header_files  = [
      "frameworks/BJLiveBase.framework/Versions/A/Headers/**/*.h"
    ]
    ss.vendored_frameworks  = [
      "frameworks/BJLiveBase.framework"
    ]
    ss.frameworks           = [
      "CoreGraphics",
      "CoreTelephony",
      "Foundation",
      "UIKit",
      "WebKit",
      "AVFoundation",
      "Photos",
      "CoreServices",
      "Security",
      "SystemConfiguration",
      "CFNetwork",
      "CoreFoundation"
    ]
    ss.libraries            = [
      "z",
      "system"
    ]
  end

  s.subspec "BJYRTCEngine" do |ss|
    ss.vendored_frameworks  = [
      "frameworks/BJYRTCEngine.framework"
    ]
    ss.frameworks            = [
      "CFNetwork",
      "AVFoundation",
      "Security",
      "AudioToolbox",
      "VideoToolbox",
      "CoreVideo",
      "CoreMedia",
      "MediaPlayer",
      "CoreServices",
      "OpenGLES",
      "CoreMotion"
    ]
    ss.libraries             = [
      "icucore",
      "bz2",
      "z",
      "c++"
    ]
    ss.dependency "BaijiaYun/BJYIJKMediaPlayer"
    ss.dependency "BaijiaYun/BJRTCSDK"
    ss.dependency "BaijiaYun/BJYWebRTC"
    ss.dependency 'BRTC', '~> 0.0.11'
  end

  s.subspec "BJYWebRTC" do |ss|
    ss.vendored_frameworks  = [
      "frameworks/BJYWebRTC.framework"
    ]
  end

  s.subspec "BJRTCSDK" do |ss|
    ss.vendored_frameworks  = [
      "frameworks/BJRTCSDK.framework"
    ]
  end

  s.subspec "BJYIJKMediaPlayer" do |ss|
    ss.source_files         = [
      "frameworks/BJYIJKMediaFramework.framework/**/*.h"
    ]
    ss.public_header_files  = [
      "frameworks/BJYIJKMediaFramework.framework/**/*.h"
    ]
    ss.vendored_frameworks  = [
      "frameworks/BJYIJKMediaFramework.framework"
    ]
    ss.frameworks           = [
      "AVFoundation",
      "GLKit",
      "VideoToolbox"
    ]
    ss.libraries            = [
      "c++",
      "z",
      "bz2"
    ]
  end

  s.subspec "Tencent" do |ss|
    ss.dependency               "TXLiteAVSDK_TRTC", "~> 7.2.8956"
  end

  s.subspec "Agora" do |ss|
    ss.dependency               "AgoraRtcEngine_iOS", "2.9.0.104"
  end

  s.subspec "_BJLiveCore" do |ss|
    ss.preserve_paths       = [
      "LICENSE",
      "frameworks/BJHLMediaPlayer.framework",
      "frameworks/EmbedFrameworks.sh",
      "frameworks/ClearArchsFromFrameworks.sh",
    ]
    ss.source_files         = [
      "frameworks/BJLiveCore.framework/Versions/A/Headers/**/*.h",
    ]
    ss.public_header_files  = [
      "frameworks/BJLiveCore.framework/Versions/A/Headers/**/*.h"
    ]
    ss.vendored_frameworks  = [
      "frameworks/BJLiveCore.framework"
    ]
    ss.frameworks           = [
      "CoreGraphics",
      "CoreTelephony",
      "Foundation",
      "UIKit",
      "WebKit",
      "AVFoundation",
      "GLKit",
      "VideoToolbox"
    ]
    ss.libraries            = [
      "icucore",
      "c++",
      "z",
      "resolv"
    ]
    ss.dependency "BaijiaYun/_BJHLMediaPlayer"
  end

  s.subspec "_BJHLMediaPlayer" do |ss|
    ss.source_files         = [
      "frameworks/BJHLMediaPlayer.framework/**/gsx_rtc_types.h"
    ]
    ss.public_header_files  = [
      "frameworks/BJHLMediaPlayer.framework/**/gsx_rtc_types.h",
    ]
  end

  s.subspec "_BJLiveUI" do |ss|
    ss.source_files         = [
      "frameworks/BJLiveUI.framework/Versions/A/Headers/**/*.h"
    ]
    ss.public_header_files  = [
      "frameworks/BJLiveUI.framework/Versions/A/Headers/**/*.h"
    ]
    ss.resources            = [
      "frameworks/BJLiveUI.framework/Versions/A/Resources/BJLiveUIMedia.bundle",
      "frameworks/BJLiveUI.framework/Versions/A/Resources/BJLSurfaceClass.bundle",
      "frameworks/BJLiveUI.framework/Versions/A/Resources/BJLInteractiveClass.bundle"
    ]
    ss.vendored_frameworks  = [
      "frameworks/BJLiveUI.framework"
    ]
    ss.frameworks = [
      "CoreGraphics",
      "Foundation",
      "CoreServices",
      "Photos",
      "PhotosUI",
      "SafariServices",
      "UIKit",
      "WebKit",
      "SpriteKit"
    ]
  end

  s.subspec "_BJLSellUI" do |ss|
    ss.source_files         = [
      "frameworks/BJLSellUI.framework/Versions/A/Headers/**/*.h"
    ]
    ss.public_header_files  = [
      "frameworks/BJLSellUI.framework/Versions/A/Headers/**/*.h"
    ]
    ss.resources            = [
      "frameworks/BJLSellUI.framework/Versions/A/Resources/BJLSellUI.bundle",
    ]
    ss.vendored_frameworks  = [
      "frameworks/BJLSellUI.framework"
    ]
    ss.frameworks = [
      "CoreGraphics",
      "Foundation",
      "CoreServices",
      "Photos",
      "PhotosUI",
      "SafariServices",
      "UIKit",
      "WebKit",
      "SpriteKit"
    ]
  end

  s.subspec "_BJVideoPlayerCore" do |ss|
    ss.source_files         = [
      "frameworks/BJVideoPlayerCore.framework/Versions/A/Headers/**/*.h"
    ]
    ss.public_header_files  = [
      "frameworks/BJVideoPlayerCore.framework/Versions/A/Headers/**/*.h"
    ]
    ss.vendored_frameworks  = [
      "frameworks/BJVideoPlayerCore.framework"
    ]
    ss.frameworks           = [
      "Foundation",
      "UIKit",
      "CoreTelephony",
      "CoreMedia",
      "AVFoundation",
      "AVKit",
      "WebKit"
    ]
  end

  s.subspec "_BJVideoPlayerUI" do |ss|
    ss.source_files         = [
      "frameworks/BJVideoPlayerUI.framework/Versions/A/Headers/**/*.h"
    ]
    ss.public_header_files  = [
      "frameworks/BJVideoPlayerUI.framework/Versions/A/Headers/**/*.h"
    ]
    ss.resource             = [
      "frameworks/BJVideoPlayerUI.framework/Versions/A/Resources/BJVideoPlayerUI.bundle"
    ]
    ss.vendored_frameworks  = [
      "frameworks/BJVideoPlayerUI.framework"
    ]
    ss.frameworks           = [
      "CoreGraphics",
      "Foundation",
      "CoreServices",
      "Photos",
      "UIKit",
      "CoreTelephony",
      "CoreMedia",
      "AVFoundation",
      "AVKit",
      "MediaPlayer"
    ]
  end

  s.subspec "_BJPlaybackUI" do |ss|
    ss.source_files         = [
      "frameworks/BJPlaybackUI.framework/Versions/A/Headers/**/*.h"
    ]
    ss.public_header_files  = [
      "frameworks/BJPlaybackUI.framework/Versions/A/Headers/**/*.h"
    ]
    ss.resource             = [
      "frameworks/BJPlaybackUI.framework/Versions/A/Resources/BJPlaybackUI.bundle"
    ]
    ss.vendored_frameworks  = [
      "frameworks/BJPlaybackUI.framework"
    ]
    ss.frameworks           = [
      "CoreGraphics",
      "Foundation",
      "CoreServices",
      "Photos",
      "UIKit",
      "CoreTelephony",
      "CoreMedia",
      "AVFoundation",
      "AVKit",
      "MediaPlayer"
    ]
  end

end
