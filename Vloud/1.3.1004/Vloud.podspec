
Pod::Spec.new do |s|
  s.name                      = 'Vloud'
  s.version                   = '1.3.1004'
  s.summary                   = 'Vloud Framework'

  s.source                    = { 
    :http => 'https://downsdk.baijiayun.com/ios/Vloud/Vloud-1.3.1004.zip', 
    :sha256 => '7e13350081fd60cd9fb40185b0eafc93891b88ae8f0ae3ba57d50b995122998c'
  }

  s.description               = <<-DESC
                                 Vloud Framework
                                 DESC

  s.homepage                  = 'https://www.baijiayun.com'
  s.author                    = { 'liuwei' => 'liuwei@baijiayun.com' }

  s.ios.deployment_target     = '9.0'

  s.source_files              = 'Vloud.framework/Headers/*.h'
  s.public_header_files       = "Vloud.framework/Headers/*.h"
  s.vendored_frameworks       = "Vloud.framework"
  
  s.user_target_xcconfig      = {
      'ENABLE_BITCODE' => 'NO'
  }

end
