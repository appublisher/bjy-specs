
Pod::Spec.new do |s|
  s.name                      = 'Vloud'
  s.version                   = '1.0.1'
  s.summary                   = 'Vloud Framework'

  s.source                    = { 
    :http => 'https://downsdk.baijiayun.com/ios/Vloud/Vloud-1.0.1.zip', 
    :sha256 => 'd637fd012bfbaaee685224a2960332901b09792a720d9e5b560216287257c3f2'
  }

  s.description               = <<-DESC
                                Vloud Framework
                                 DESC

  s.homepage                  = 'https://www.baijiayun.com'
  s.author                    = { 'liuwei' => 'liuwei@baijiayun.com' }

  s.ios.deployment_target     = '9.0'

  s.source_files              = 'Vloud.framework/Headers/*.h'
  s.public_header_files       = "Vloud.framework/Headers/*.h"
  s.vendored_frameworks       = "Vloud.framework"
  
  s.user_target_xcconfig      = {
      'ENABLE_BITCODE' => 'NO'
  }

end
