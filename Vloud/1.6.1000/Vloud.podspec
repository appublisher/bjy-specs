
Pod::Spec.new do |s|
  s.name                      = 'Vloud'
  s.version                   = '1.6.1000'
  s.summary                   = 'Vloud Framework'

  s.source                    = { 
    :http => 'https://downsdk.baijiayun.com/ios/Vloud/Vloud-1.6.1000.zip', 
    :sha256 => '67a8ce7192225da4c499995018fb598a3b4ba3409e8cf2df36474ddd8182c111'
  }

  s.description               = <<-DESC
                                 Vloud Framework
                                 DESC

  s.homepage                  = 'https://www.baijiayun.com'
  s.author                    = { 'liuwei' => 'liuwei@baijiayun.com' }

  s.ios.deployment_target     = '9.0'

  s.source_files              = 'Vloud.framework/Headers/*.h'
  s.public_header_files       = "Vloud.framework/Headers/*.h"
  s.vendored_frameworks       = "Vloud.framework"
  
  s.user_target_xcconfig      = {
      'ENABLE_BITCODE' => 'NO'
  }

end
