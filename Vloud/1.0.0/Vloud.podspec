
Pod::Spec.new do |s|
  s.name                      = 'Vloud'
  s.version                   = '1.0.0'
  s.summary                   = 'Vloud Framework'

  s.source                    = { 
    :http => 'https://downsdk.baijiayun.com/ios/Vloud/Vloud-1.0.0.zip', 
    :sha256 => '9c41fd1dd00aefbebb0144553ca1960f0a89f5c7646246216d9fe95119ade3b5'
  }

  s.description               = <<-DESC
                                Vloud Framework
                                 DESC

  s.homepage                  = 'https://www.baijiayun.com'
  s.author                    = { 'liuwei' => 'liuwei@baijiayun.com' }

  s.ios.deployment_target     = '9.0'

  s.source_files              = 'Vloud.framework/Headers/*.h'
  s.public_header_files       = "Vloud.framework/Headers/*.h"
  s.vendored_frameworks       = "Vloud.framework"
  
  s.user_target_xcconfig      = {
      'ENABLE_BITCODE' => 'NO'
  }

end
