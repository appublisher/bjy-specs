
Pod::Spec.new do |s|
  s.name                      = 'Vloud'
  s.version                   = '1.0.9'
  s.summary                   = 'Vloud Framework'

  s.source                    = { 
    :http => 'https://downsdk.baijiayun.com/ios/Vloud/Vloud-1.0.9.zip', 
    :sha256 => 'a6e28fc83dba8630cd99876822892828c24e9d8a1503457c2aabbed8ec9314e8'
  }

  s.description               = <<-DESC
                                 Vloud Framework
                                 DESC

  s.homepage                  = 'https://www.baijiayun.com'
  s.author                    = { 'liuwei' => 'liuwei@baijiayun.com' }

  s.ios.deployment_target     = '9.0'

  s.source_files              = 'Vloud.framework/Headers/*.h'
  s.public_header_files       = "Vloud.framework/Headers/*.h"
  s.vendored_frameworks       = "Vloud.framework"
  
  s.user_target_xcconfig      = {
      'ENABLE_BITCODE' => 'NO'
  }

end
