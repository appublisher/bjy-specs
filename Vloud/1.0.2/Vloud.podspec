
Pod::Spec.new do |s|
  s.name                      = 'Vloud'
  s.version                   = '1.0.2'
  s.summary                   = 'Vloud Framework'

  s.source                    = { 
    :http => 'https://downsdk.baijiayun.com/ios/Vloud/Vloud-1.0.2.zip', 
    :sha256 => 'b7c22641ae6d6c24fa98b4ecd8f4a667d10575bd729135e624d790be02410f32'
  }

  s.description               = <<-DESC
                                Vloud Framework
                                 DESC

  s.homepage                  = 'https://www.baijiayun.com'
  s.author                    = { 'liuwei' => 'liuwei@baijiayun.com' }

  s.ios.deployment_target     = '9.0'

  s.source_files              = 'Vloud.framework/Headers/*.h'
  s.public_header_files       = "Vloud.framework/Headers/*.h"
  s.vendored_frameworks       = "Vloud.framework"
  
  s.user_target_xcconfig      = {
      'ENABLE_BITCODE' => 'NO'
  }

end
