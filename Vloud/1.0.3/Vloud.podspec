
Pod::Spec.new do |s|
  s.name                      = 'Vloud'
  s.version                   = '1.0.3'
  s.summary                   = 'Vloud Framework'

  s.source                    = { 
    :http => 'https://downsdk.baijiayun.com/ios/Vloud/Vloud-1.0.3.zip', 
    :sha256 => 'dcd44ed62cdd2400d9ca1fdfe72b668bb0db4766bfb6f7817cd28fe37f001a99'
  }

  s.description               = <<-DESC
                                 Vloud Framework
                                 DESC

  s.homepage                  = 'https://www.baijiayun.com'
  s.author                    = { 'liuwei' => 'liuwei@baijiayun.com' }

  s.ios.deployment_target     = '9.0'

  s.source_files              = 'Vloud.framework/Headers/*.h'
  s.public_header_files       = "Vloud.framework/Headers/*.h"
  s.vendored_frameworks       = "Vloud.framework"
  
  s.user_target_xcconfig      = {
      'ENABLE_BITCODE' => 'NO'
  }

end
