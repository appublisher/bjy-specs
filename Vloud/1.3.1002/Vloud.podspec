
Pod::Spec.new do |s|
  s.name                      = 'Vloud'
  s.version                   = '1.3.1002'
  s.summary                   = 'Vloud Framework'

  s.source                    = { 
    :http => 'https://downsdk.baijiayun.com/ios/Vloud/Vloud-1.3.1002.zip', 
    :sha256 => '42c7ef8b0f1842595afe730961f12234c5c89fcc82ac0ad6a62f2b8a3c961044'
  }

  s.description               = <<-DESC
                                 Vloud Framework
                                 DESC

  s.homepage                  = 'https://www.baijiayun.com'
  s.author                    = { 'liuwei' => 'liuwei@baijiayun.com' }

  s.ios.deployment_target     = '9.0'

  s.source_files              = 'Vloud.framework/Headers/*.h'
  s.public_header_files       = "Vloud.framework/Headers/*.h"
  s.vendored_frameworks       = "Vloud.framework"
  
  s.user_target_xcconfig      = {
      'ENABLE_BITCODE' => 'NO'
  }

end
