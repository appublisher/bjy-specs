#
# Be sure to run `pod lib lint BRTM.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'BRTM'
  s.version          = '0.0.1'
  s.summary          = 'BRTM framework SDK'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = <<-DESC
TODO: Add long description of the pod here.
                       DESC

  s.homepage         = 'https://www.baijiayun.com'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { '辛亚鹏' => 'xinyapeng@baijiayun.com' }

  s.source           = {
    :http => 'https://downsdk.baijiayun.com/ios/BRTM/BRTM-0.0.1.zip',
    :sha256 => '5c2bb20013fc9da9242583d1873e5a13bbfecbbb5bd8d32ae97f5a230f8bf0b2'
  }

  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.ios.deployment_target = '9.0'

  s.pod_target_xcconfig = {
      "GCC_PREPROCESSOR_DEFINITIONS" => "$(inherited) BRTM_NAME=#{s.name} BRTM_VERSION=#{s.version}",
  }

  s.source_files              = "BRTM.framework/Versions/A/Headers/*.h"
  s.public_header_files       = "BRTM.framework/Versions/A/Headers/*.h"
  s.vendored_frameworks       = "BRTM.framework"
  
  s.dependency 'BaijiaYun/BJLiveBase'
end


