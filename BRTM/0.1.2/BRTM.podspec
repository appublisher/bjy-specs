#
# Be sure to run `pod lib lint BRTM.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'BRTM'
  s.version          = '0.1.2'
  s.summary          = 'BRTM framework SDK'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = <<-DESC
TODO: Add long description of the pod here.
                       DESC

  s.homepage         = 'https://www.baijiayun.com'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { '辛亚鹏' => 'xinyapeng@baijiayun.com' }

  s.source           = {
    :http => 'https://downsdk.baijiayun.com/ios/BRTM/BRTM-latest.zip',
    :sha256 => '6095fb6a3d0016cbf2d47b5342e8922c08cfd40d63296700f36d326be9d5a7a6'
  }

  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.ios.deployment_target = '9.0'

  s.pod_target_xcconfig = {
      "GCC_PREPROCESSOR_DEFINITIONS" => "$(inherited) BRTM_NAME=#{s.name} BRTM_VERSION=#{s.version}",
  }

  s.source_files              = "BRTM.framework/Versions/A/Headers/*.h"
  s.public_header_files       = "BRTM.framework/Versions/A/Headers/*.h"
  s.resource                  = "BRTM.framework/Versions/A/Resources/BRTM.bundle"
  s.vendored_frameworks       = "BRTM.framework"
  
  s.dependency 'BaijiaYun/BJLiveBase'
end


