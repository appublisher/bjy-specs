
Pod::Spec.new do |s|
  s.name                      = 'BRTC'
  s.version                   = '0.0.12'
  s.summary                   = 'BRTC Framework'

  s.description               = <<-DESC
                                BRTC Framework
                                DESC

  s.homepage                  = 'https://www.baijiayun.com'
  s.author                    = { 'liuwei' => 'liuwei@baijiayun.com' }

  s.source                    = { 
    :http => 'https://downsdk.baijiayun.com/ios/BRTC/BRTC-0.0.12.zip', 
    :sha256 => '232b40d1ea8820b8b1cbc41b55752986af7e676b3ad3f4b93c43e58da5613de2'
  }

  s.ios.deployment_target     = '9.0'

  s.source_files              = 'BRTC.framework/Headers/*.h'
  s.public_header_files       = "BRTC.framework/Headers/*.h"
  s.vendored_frameworks       = "BRTC.framework"

  s.dependency                'TXLiteAVSDK_TRTC', '~> 7.2.8956'
  s.dependency                'Vloud', '~> 1.0.6'
  
  s.user_target_xcconfig      = {
      'ENABLE_BITCODE' => 'NO'
  }

end
