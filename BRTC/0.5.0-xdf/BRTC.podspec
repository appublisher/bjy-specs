
Pod::Spec.new do |s|
  s.name                      = 'BRTC'
  s.version                   = '0.5.0-xdf'
  s.summary                   = 'BRTC Framework'

  s.description               = <<-DESC
                                BRTC Framework
                                DESC

  s.homepage                  = 'https://www.baijiayun.com'
  s.author                    = { 'liuwei' => 'liuwei@baijiayun.com' }

  s.source                    = { 
    :http => 'https://downsdk.baijiayun.com/ios/BRTC/BRTC-0.5.0-xdf.zip', 
    :sha256 => '2f41cf20b1c3052d4d7055999ca7800016b8b5ac8d90647987fe51d7f0368d9f'
  }

  s.ios.deployment_target     = '9.0'

  s.source_files              = 'BRTC.framework/Headers/*.h'
  s.public_header_files       = "BRTC.framework/Headers/*.h"
  s.vendored_frameworks       = "BRTC.framework"

  s.dependency                'TXLiteAVSDK_TRTC', '~> 7.2.8980'
  s.dependency                'Vloud', '1.6.1000'
  
  s.user_target_xcconfig      = {
      'ENABLE_BITCODE' => 'NO'
  }

end
