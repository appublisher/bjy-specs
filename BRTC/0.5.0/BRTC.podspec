
Pod::Spec.new do |s|
  s.name                      = 'BRTC'
  s.version                   = '0.5.0'
  s.summary                   = 'BRTC Framework'

  s.description               = <<-DESC
                                BRTC Framework
                                DESC

  s.homepage                  = 'https://www.baijiayun.com'
  s.author                    = { 'liuwei' => 'liuwei@baijiayun.com' }

  s.source                    = { 
    :http => 'https://downsdk.baijiayun.com/ios/BRTC/BRTC-0.5.0.zip', 
    :sha256 => '2c6151ff8711604890598556c46f1948d4d98d57f63407ae210212ed96c3250e'
  }

  s.ios.deployment_target     = '9.0'

  s.source_files              = 'BRTC.framework/Headers/*.h'
  s.public_header_files       = "BRTC.framework/Headers/*.h"
  s.vendored_frameworks       = "BRTC.framework"

 # s.dependency                'TXLiteAVSDK_TRTC', '~> 8.4.9944'
  s.dependency                'Vloud', '1.6.1000'
  
  s.user_target_xcconfig      = {
      'ENABLE_BITCODE' => 'NO'
  }

end
