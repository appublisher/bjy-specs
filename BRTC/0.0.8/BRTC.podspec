
Pod::Spec.new do |s|
  s.name                      = 'BRTC'
  s.version                   = '0.0.8'
  s.summary                   = 'BRTC Framework'

  s.description               = <<-DESC
                                BRTC Framework
                                DESC

  s.homepage                  = 'https://www.baijiayun.com'
  s.author                    = { 'liuwei' => 'liuwei@baijiayun.com' }

  s.source                    = { 
    :http => 'https://downsdk.baijiayun.com/ios/BRTC/BRTC-0.0.8.zip', 
    :sha256 => '3dc4cdff7032e31738b0424faa87c99de19712aaebcb804de8f4c43fbe26a967'
  }

  s.ios.deployment_target     = '9.0'

  s.source_files              = 'BRTC.framework/Headers/*.h'
  s.public_header_files       = "BRTC.framework/Headers/*.h"
  s.vendored_frameworks       = "BRTC.framework"

  s.dependency                'TXLiteAVSDK_TRTC', '~> 7.2.8956'
  s.dependency                'Vloud', '~> 1.0.2'
  
  s.user_target_xcconfig      = {
      'ENABLE_BITCODE' => 'NO'
  }

end
