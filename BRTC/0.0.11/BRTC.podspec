
Pod::Spec.new do |s|
  s.name                      = 'BRTC'
  s.version                   = '0.0.11'
  s.summary                   = 'BRTC Framework'

  s.description               = <<-DESC
                                BRTC Framework
                                DESC

  s.homepage                  = 'https://www.baijiayun.com'
  s.author                    = { 'liuwei' => 'liuwei@baijiayun.com' }

  s.source                    = { 
    :http => 'https://downsdk.baijiayun.com/ios/BRTC/BRTC-0.0.11.zip', 
    :sha256 => 'c229188d77ff4c646e77a121c7d9afb03b630f1ad6426d461f9a711d1391d8da'
  }

  s.ios.deployment_target     = '9.0'

  s.source_files              = 'BRTC.framework/Headers/*.h'
  s.public_header_files       = "BRTC.framework/Headers/*.h"
  s.vendored_frameworks       = "BRTC.framework"

  s.dependency                'TXLiteAVSDK_TRTC', '~> 7.2.8956'
  s.dependency                'Vloud', '~> 1.0.3'
  
  s.user_target_xcconfig      = {
      'ENABLE_BITCODE' => 'NO'
  }

end
