
Pod::Spec.new do |s|
  s.name                      = 'BRTC'
  s.version                   = '0.1.1'
  s.summary                   = 'BRTC Framework'

  s.description               = <<-DESC
                                BRTC Framework
                                DESC

  s.homepage                  = 'https://www.baijiayun.com'
  s.author                    = { 'liuwei' => 'liuwei@baijiayun.com' }

  s.source                    = { 
    :http => 'https://downsdk.baijiayun.com/ios/BRTC/BRTC-0.1.1.zip', 
    :sha256 => '487f8a159864f79abf411fa9002e33931c8b59a704866cd8c03fec2ed726a2b1'
  }

  s.ios.deployment_target     = '9.0'

  s.source_files              = 'BRTC.framework/Headers/*.h'
  s.public_header_files       = "BRTC.framework/Headers/*.h"
  s.vendored_frameworks       = "BRTC.framework"

  s.dependency                'TXLiteAVSDK_TRTC', '~> 7.2.8956'
  s.dependency                'Vloud', '~> 1.2.1002'
  
  s.user_target_xcconfig      = {
      'ENABLE_BITCODE' => 'NO'
  }

end
