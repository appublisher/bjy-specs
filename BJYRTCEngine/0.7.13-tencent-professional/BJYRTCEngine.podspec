Pod::Spec.new do |s|
  s.name = "BJYRTCEngine"
  s.version = "0.7.13-tencent-professional"
  s.summary = "BJYRTCEngine desc"
  s.authors = {"tianbo"=>"tianbo@baijiayun.com", "liuwei"=>"liuwei@baijiayun.com"}
  s.homepage = "www.baijiayun.com"
  s.requires_arc = true
  s.source = {
      :git => "https://git.baijiashilian.com/open-ios/bjyrtcengine.git",
      :tag => s.version.to_s
  }

  s.default_subspecs = ["static", "BJYIJKMediaPlayer"]

  s.subspec 'static' do |ss|
      ss.dependency 'BJYRTCEngine/nothirdparty'
      ss.dependency 'BJYRTCEngine/tencent'
      ss.dependency 'BJYRTCEngine/agora'
  end
  
  s.subspec 'BJYIJKMediaPlayer' do |ss|
      ss.ios.source_files        = "BJYRTCEngine/BJYIJKMediaFramework.framework/**/*.h"
      ss.ios.public_header_files = "BJYRTCEngine/BJYIJKMediaFramework.framework/**/*.h"
      ss.ios.preserve_paths      = "BJYRTCEngine/BJYIJKMediaFramework.framework"
      ss.ios.vendored_frameworks = "BJYRTCEngine/BJYIJKMediaFramework.framework"
  end

  s.subspec 'nothirdparty' do |ss|
      ss.ios.vendored_frameworks   = ["BJYRTCEngine/BJRTCSDK.framework", "BJYRTCEngine/BJYRTCEngine.framework", "BJYRTCEngine/BJYWebRTC.framework"]
  end

  s.subspec 'tencent' do |ss|
      ss.dependency               'TXLiteAVSDK_Professional', '7.5.9309'
  end

  s.subspec 'agora' do |ss|
      ss.dependency               'AgoraRtcEngine_iOS', '2.9.0.104'
  end

  s.ios.deployment_target    = '9.0'
  s.xcconfig                 = {"ENABLE_BITCODE" => "NO"}
  s.frameworks               = 'CFNetwork', 'AVFoundation', 'Security', 'AudioToolbox', 'VideoToolbox', 'CoreVideo', 'CoreMedia', 'MediaPlayer', 'MobileCoreServices', 'OpenGLES', 'CoreMotion'
  s.libraries                = 'icucore', 'bz2', 'z', 'c++'

end
