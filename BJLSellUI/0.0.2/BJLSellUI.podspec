#
# Be sure to run `pod lib lint BJLSellUI.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'BJLSellUI'
  s.version          = '0.0.2'
  s.summary          = 'BJLSellUI SDK'
  s.description      = 'BJLSellUI SDK'

  s.ios.deployment_target = '9.0'
  
  #

  s.homepage         = 'https://www.baijiayun.com/'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { '辛亚鹏' => 'xinyapeng@baijiayun.com' }
  s.source           = { :git => 'https://git.baijiashilian.com/open-ios/BJLSellUI.git', :tag => s.version.to_s }
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'
  
  s.pod_target_xcconfig = {
      "CLANG_ALLOW_NON_MODULAR_INCLUDES_IN_FRAMEWORK_MODULES" => "YES", # requies both `user_target_xcconfig` and `pod_target_xcconfig`
      "CLANG_ANALYZER_LOCALIZABILITY_NONLOCALIZED" => "NO",
      "GCC_PREPROCESSOR_DEFINITIONS" => "$(inherited) BJLSELLUI_NAME=#{s.name} BJLSELLUI_VERSION=#{s.version}"
  }

  s.user_target_xcconfig = {
     "CLANG_ALLOW_NON_MODULAR_INCLUDES_IN_FRAMEWORK_MODULES" => "YES" # requies both `user_target_xcconfig` and `pod_target_xcconfig`
  }

  s.default_subspecs = ['static']

  ## 默认集成framework
  s.subspec 'static' do |ss|
      ## framework
      ss.ios.preserve_paths       = 'frameworks/BJLSellUI.framework'
      ss.ios.source_files         = 'frameworks/BJLSellUI.framework/Versions/A/Headers/**/*.h'
      ss.ios.public_header_files  = 'frameworks/BJLSellUI.framework/Versions/A/Headers/**/*.h'
      ss.ios.resource             = 'frameworks/BJLSellUI.framework/Versions/A/Resources/BJLSellUI.bundle'
      ss.ios.vendored_frameworks  = 'frameworks/BJLSellUI.framework'
  end


  ## 集成源码
  s.subspec 'static.source' do |ss|
     ss.source_files = 'BJLSellUI/Classes/**/*'
     ss.resource_bundles = {
         'BJLSellUI' => ['BJLSellUI/**/*.xcassets']
     }
  end
  
  s.dependency "BJLiveBase",                          "~> 2.9.1"
  s.dependency "BJLiveBase/Base",                     "~> 2.9.1"
  s.dependency "BJLiveBase/Auth",                     "~> 2.9.1"
  s.dependency "BJLiveBase/HUD",                      "~> 2.9.1"
  s.dependency "BJLiveBase/Networking",               "~> 2.9.1"
  s.dependency "BJLiveBase/WebImage/AFNetworking",    "~> 2.9.1"

  s.dependency "BJLiveCore", "~> 2.9.1"
  
end
