#
# Be sure to run `pod lib lint BJHL-VideoPlayer-Manager.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
    s.name          = 'BJVideoPlayerCore'
    s.version       = '2.10.1'
    s.summary       = 'BJVideoPlayerCore'
    s.description   = "BJVideoPlayerCore SDK for iOS."
    
    s.homepage      = 'https://www.baijiayun.com/'
    s.license       = { :type => 'MIT', :file => 'LICENSE' }
    s.author        = { '辛亚鹏' => 'xinyapeng@baijiahulian.com' }
    s.source           = { :git => 'https://git.baijiashilian.com/open-ios/BJVideoPlayerCore.git', :tag => s.version.to_s }
    
    s.platform      = :ios, "9.0"
    s.ios.deployment_target = "9.0"
    
    s.requires_arc = true
    
    s.pod_target_xcconfig = {
        "CLANG_ALLOW_NON_MODULAR_INCLUDES_IN_FRAMEWORK_MODULES" => "YES", # requies both `user_target_xcconfig` and `pod_target_xcconfig`
        "GCC_PREPROCESSOR_DEFINITIONS" => "$(inherited) BJVIDEOPLAYERCORE_NAME=#{s.name} BJVIDEOPLAYERCORE_VERSION=#{s.version}"
    }
    s.user_target_xcconfig = {
        "CLANG_ALLOW_NON_MODULAR_INCLUDES_IN_FRAMEWORK_MODULES" => "YES" # requies both `user_target_xcconfig` and `pod_target_xcconfig`
    }
    
    s.default_subspecs = 'static'

    s.subspec 'static' do |ss|
        ss.dependency "BJVideoPlayerCore/core"
        ss.dependency "BJLiveCore", "~> 2.10.2"
    end

    s.subspec 'nothirdparty' do |ss|
        ss.dependency "BJVideoPlayerCore/core"
        ss.dependency "BJLiveCore/nothirdparty", "~> 2.10.2"
    end

    s.subspec 'tencent' do |ss|
        ss.dependency "BJVideoPlayerCore/core"
        ss.dependency "BJLiveCore/tencent", "~> 2.10.2"
    end

    s.subspec 'agora' do |ss|
        ss.dependency "BJVideoPlayerCore/core"
        ss.dependency "BJLiveCore/agora", "~> 2.10.2"
    end
    
    s.subspec 'core' do |ss|
        ss.preserve_paths       = ['BJVideoPlayerCore/BJVideoPlayerCore.framework', 'frameworks/EmbedFrameworks.sh']
        ss.source_files         = 'BJVideoPlayerCore/BJVideoPlayerCore.framework/Versions/A/Headers/**/*.h'
        ss.public_header_files  = 'BJVideoPlayerCore/BJVideoPlayerCore.framework/Versions/A/Headers/**/*.h'
        ss.vendored_frameworks  = 'BJVideoPlayerCore/BJVideoPlayerCore.framework'
        ss.frameworks           = ['Foundation', 'UIKit', 'CoreTelephony', 'CoreMedia', 'AVFoundation', 'AVKit', 'WebKit']
        ss.dependency "BJLiveBase/Download",'~> 2.10.0'
    end  
end
