Pod::Spec.new do |s|
    
    s.name          = "BJLiveCore"
    s.version       = "2.10.2"
    s.summary       = "BJLiveCore SDK."
    s.description   = "BJLiveCore SDK for iOS."
    
    s.homepage      = "https://www.baijiayun.com/"
    s.license       = "MIT"
    s.author        = { "MingLQ" => "minglq.9@gmail.com" }
    
    s.platform      = :ios, "9.0"
    s.ios.deployment_target = "9.0"
    
    s.source = {
        :git => 'https://git.baijiashilian.com/open-ios/BJLiveCore.git',
        :tag => s.version.to_s
    }
    
    s.requires_arc = true
    
    s.pod_target_xcconfig = {
        "CLANG_ALLOW_NON_MODULAR_INCLUDES_IN_FRAMEWORK_MODULES" => "YES", # requies both `user_target_xcconfig` and `pod_target_xcconfig`
        "GCC_PREPROCESSOR_DEFINITIONS" => "$(inherited) BJLIVECORE_NAME=#{s.name} BJLIVECORE_VERSION=#{s.version}"
    }
    s.user_target_xcconfig = {
        "CLANG_ALLOW_NON_MODULAR_INCLUDES_IN_FRAMEWORK_MODULES" => "YES" # requies both `user_target_xcconfig` and `pod_target_xcconfig`
    }
    
    s.default_subspecs = ['static']

    # 默认集成全部第三方 SDK
    s.subspec 'static' do |ss|
        ss.dependency 'BJLiveCore/core'
        ss.dependency "BJYRTCEngine/static",                      "~> 0.7.11"
    end

    # 不集成任何第三方 SDK
    s.subspec 'nothirdparty' do |ss|
        ss.dependency 'BJLiveCore/core'
        ss.dependency "BJYRTCEngine/nothirdparty",                "~> 0.7.11"
    end

    # 集成腾讯的 SDK
    s.subspec 'tencent' do |ss|
        ss.dependency 'BJLiveCore/core'
        ss.dependency "BJYRTCEngine/nothirdparty",                "~> 0.7.11"
        ss.dependency "BJYRTCEngine/tencent",                     "~> 0.7.11"
    end

    # 集成声网的 SDK
    s.subspec 'agora' do |ss|
        ss.dependency 'BJLiveCore/core'
        ss.dependency "BJYRTCEngine/nothirdparty",                "~> 0.7.11"
        ss.dependency "BJYRTCEngine/agora",                       "~> 0.7.11"
    end

    # core SDK
    s.subspec 'core' do |ss|
        ss.preserve_paths       = 'frameworks/BJLiveCore.framework'
        ss.source_files         = 'frameworks/BJLiveCore.framework/Versions/A/Headers/**/*.h'
        ss.public_header_files  = 'frameworks/BJLiveCore.framework/Versions/A/Headers/**/*.h'
        ss.vendored_frameworks  = 'frameworks/BJLiveCore.framework'
        ss.libraries            = ["resolv"]
        ss.frameworks           = ['CoreGraphics', 'CoreTelephony', 'Foundation', 'UIKit', 'WebKit']
        ss.dependency 'BJLiveCore/BJHLMediaPlayer'
        ss.dependency "BJYRTCEngine/BJYIJKMediaPlayer",     "~> 0.7.11"
    end
    
    # AVSDK
    s.subspec 'BJHLMediaPlayer' do |ss|
        ss.user_target_xcconfig = {
            "ENABLE_BITCODE" => "NO", # requies `user_target_xcconfig`, TODO: "YES"
        }
        ss.preserve_paths       = ["frameworks/BJHLMediaPlayer.framework", "frameworks/EmbedFrameworks.sh"]
        ss.source_files         = ["frameworks/BJHLMediaPlayer.framework/**/gsx_rtc_types.h"]
        ss.private_header_files = ["frameworks/BJHLMediaPlayer.framework/**/gsx_rtc_types.h"]
        ss.frameworks           = ['AVFoundation', 'GLKit', 'VideoToolbox']
        ss.libraries            = ['icucore', 'c++', 'z']
    end
    
    s.dependency "BJLiveBase",                         "~> 2.10.0"
    s.dependency "BJLiveBase/Base",                    "~> 2.10.0"
    s.dependency "BJLiveBase/Networking",              "~> 2.10.0"
    s.dependency "BJLiveBase/Networking+BaijiaYun",    "~> 2.10.0"
    s.dependency "BJLiveBase/PocketSocket/Client",     "~> 2.10.0"
    s.dependency "BJLiveBase/WebImage/BJLWebImage",    "~> 2.10.0"
    s.dependency "BJLiveBase/WebImage/AFNetworking",   "~> 2.10.0"
    s.dependency "BJLiveBase/YYModel",                 "~> 2.10.0"
    s.dependency "BJLiveBase/Auth",                    "~> 2.10.0"

end
