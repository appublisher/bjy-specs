Pod::Spec.new do |s|

    s.name          = 'BJLiveCore'
    s.version       = '1.6.0'
    s.summary       = 'BJLiveCore SDK.'
    s.description   = 'BJLiveCore SDK for iOS.'

    s.homepage      = 'http://www.baijiayun.com/'
    s.license       = 'MIT'
    s.author        = { 'MingLQ' => 'minglq.9@gmail.com' }

    s.platform      = :ios
    s.ios.deployment_target = '8.0'

    s.source = {
        :git => 'https://git.baijiashilian.com/open-ios/BJLiveCore.git',
        :tag => s.version.to_s
    }

    s.pod_target_xcconfig = {
        "CLANG_ALLOW_NON_MODULAR_INCLUDES_IN_FRAMEWORK_MODULES" => "YES", # requies both `user_target_xcconfig` and `pod_target_xcconfig`
        "GCC_PREPROCESSOR_DEFINITIONS" => "$(inherited) BJLIVECORE_NAME=#{s.name} BJLIVECORE_VERSION=#{s.version}",
    }
    s.user_target_xcconfig = {
        "CLANG_ALLOW_NON_MODULAR_INCLUDES_IN_FRAMEWORK_MODULES" => "YES" # requies both `user_target_xcconfig` and `pod_target_xcconfig`
    }

    s.default_subspecs = ['static']

    s.subspec 'static' do |ss|
        ss.ios.preserve_paths       = 'frameworks/BJLiveCore.framework'
        ss.ios.source_files         = 'frameworks/BJLiveCore.framework/Versions/A/Headers/**/*.h'
        ss.ios.public_header_files  = 'frameworks/BJLiveCore.framework/Versions/A/Headers/**/*.h'
        ss.ios.vendored_frameworks  = 'frameworks/BJLiveCore.framework'
        ss.libraries                = ["resolv"]
        ss.frameworks               = ['CoreGraphics', 'CoreTelephony', 'Foundation', 'UIKit', 'WebKit']
        ss.dependency 'BJLiveCore/BJHLMediaPlayer'
        ss.dependency 'BJLLogStat', '~> 1.0.0'
    end

    s.subspec 'BJHLMediaPlayer' do |ss|
        ss.xcconfig = {
            "ENABLE_BITCODE" => "NO", # requies `user_target_xcconfig`, TODO: "YES"
        }
        ss.libraries = ["icucore", "c++", "z"]
        ss.frameworks = ["AVFoundation", "GLKit", "VideoToolbox"]
        ss.source_files = ["frameworks/BJHLMediaPlayer.framework/**/gsx_rtc_types.h"]
        ss.private_header_files = ["frameworks/BJHLMediaPlayer.framework/**/gsx_rtc_types.h"]
        ss.preserve_paths = ["frameworks/BJHLMediaPlayer.framework", "frameworks/EmbedFrameworks.sh"]
    end

    s.dependency "BJLiveBase", "~> 1.4.0"

end
